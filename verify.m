disp('Checking +PreInterpolator dependencies')

% State package manager version then import if OK
PackageManagement.verify_self('2')
import PackageManagement.*

% Add package dependencies here
verify_package('FftTools', '2');
